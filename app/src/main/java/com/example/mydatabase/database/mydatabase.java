package com.example.mydatabase.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class mydatabase extends SQLiteAssetHelper {
    public static final String DATABASE_NAME = "simpledata.db";
    public static final Integer DATABASE_VERSION = 1;
    public mydatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
