package com.example.mydatabase.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;



public class TblDtatauser extends mydatabase {

    public static final String TBL_DTATA_USER = "Tbl_Dtata_user";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LASTNAME = "lastname";
    public static final String PHONENO="phoneno";
    public static final String EMAILS = "emails";
    public static final String GENDER="ugender";
    public static final String HOBBY="uhobby";

    public TblDtatauser(Context context) {
        super(context);
    }
    public long insertuserdetails(String name,String lsname,String phone,String email,String gender,String hobby) {

        long inserid =0;
        if (isnumberavi(phone)) {
             inserid = -1;
        } else {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(NAME, name);
            cv.put(LASTNAME, lsname);
            cv.put(PHONENO, phone);
            cv.put(EMAILS, email);
            cv.put(GENDER, gender);
            cv.put(HOBBY, hobby);
             inserid = db.insert(TBL_DTATA_USER, null, cv);
            db.close();
        }

        return inserid;
    }
    public boolean isnumberavi(String phono)
    {
        SQLiteDatabase db= getReadableDatabase();
        String qurey = "SELECT * FROM "+ TBL_DTATA_USER +" WHERE "+ PHONENO +" = ?";
        Cursor cursor = db.rawQuery(qurey,new String[]{phono});
        cursor.moveToFirst();
        boolean isnumberavi= cursor.getCount() > 0;
        cursor.close();
        db.close();
        return isnumberavi;
    }


}
