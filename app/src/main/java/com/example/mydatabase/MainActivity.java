package com.example.mydatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.mydatabase.database.TblDtatauser;
import com.example.mydatabase.database.mydatabase;

public class MainActivity extends AppCompatActivity {

    EditText nam,last,emil,pho;
    Button bt;
    RadioGroup rg;
    RadioButton rm,rf;
    CheckBox ch1,ch2,ch3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);   initreferanse();
        initvalues();
    }
    void initreferanse()
    {
        new mydatabase(MainActivity.this).getReadableDatabase();

        nam = findViewById(R.id.fname);
        last = findViewById(R.id.lname);
        emil = findViewById(R.id.email);
        bt = findViewById(R.id.btn);
        pho = findViewById(R.id.ephone);
        rm = findViewById(R.id.rb_male);
        rf = findViewById(R.id.rb_female);
        rg = findViewById(R.id.rd_gp);
        ch1 = findViewById(R.id.chk_cricket);
        ch2 = findViewById(R.id.chk_footboll);
        ch3 = findViewById(R.id.chl_hokey);
    }
    void initvalues()
    {
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nm = nam.getText().toString();
                String ls = last.getText().toString();
                String ph = pho.getText().toString();
                String em = emil.getText().toString();
                String hobbies = "";
                if (ch1.isChecked()) {
                    hobbies += "," + ch1.getText().toString();
                }
                if (ch3.isChecked()) {
                    hobbies += "," + ch3.getText().toString();
                }
                if (ch2.isChecked()) {
                    hobbies += "," + ch2.getText().toString();
                }
                if (hobbies.length() >= 0) {
                    hobbies.substring(1);
                }
                int rid = rg.getCheckedRadioButtonId();
                View rb = rg.findViewById(rid);
                int idx = rg.indexOfChild(rb);
                String gn="";
                if(idx == 1)
                {
                    gn="male";
                }
                else
                {
                    gn="female";
                }
                TblDtatauser tbluser = new TblDtatauser(MainActivity.this);
                long lastid = tbluser.insertuserdetails(nm,ls,ph,em,gn,hobbies);
                if(lastid > 0)
                {
                    Toast.makeText(getApplicationContext(),"user inserted",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"user not add",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}

